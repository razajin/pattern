package com.pattern.factory.mine.factorymethod;

/**
 * to do
 *
 * @author zhoukaifang
 * @date 2021/12/21
 */
public class I3ComputerFactory implements ComputerFactory {
    @Override
    public Computer createComputer() {
        return new I7Computer("i3","580","16G");
    }
}

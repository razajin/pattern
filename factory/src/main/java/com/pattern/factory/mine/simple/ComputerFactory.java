package com.pattern.factory.mine.simple;

/**
 * to do
 * 简单工厂是用来生产产品的，
 * 工厂模式的产品一定是一个生产过程很复杂,且他生产需要的材料是不需要用户提供的,他仅仅是提供用户一个选择的空间
 * 所以学工厂模式，举例一定要切合工厂模式的应用场景，否者理解起来会觉得工厂提高的代码复杂性。
 *
 * 工厂的优势是易于扩展，扩展的是产品类型，通过添加不同的工厂来增加产品的种类
 *
 *  具体通过，家电工厂生产空调吧
 * @author zhoukaifang
 * @date 2021/12/21
 */
public class ComputerFactory {
    public ComputerFunction createComputer(String type){
        ComputerFunction computer ;
        if ("i3".equals(type)){
            computer = new I3Computer("i3","580","8G");
        }else{
            computer = new I7Computer("i7","580","16G");
        }
        return computer;
    }
}

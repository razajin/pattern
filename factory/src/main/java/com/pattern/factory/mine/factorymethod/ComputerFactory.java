package com.pattern.factory.mine.factorymethod;

/**
 * @date 2021/12/21
 */
public interface ComputerFactory {
    public Computer createComputer();
}

package com.pattern.factory.mine.factorymethod;

/**
 * to do
 *
 * @author zhoukaifang
 * @date 2021/12/21
 */
public class User {


    /**
     * des.
     * 优点： 符合开闭原则 增加产品仅需增加工厂就可以了
     *
     * https://www.cnblogs.com/kubixuesheng/p/10353209.html
     * @author       : RazaJin
     * @date         :  18:20
     */
    public static void main(String[] args) {
        ComputerFactory computerFactory = new I3ComputerFactory();
        Computer i3 = computerFactory.createComputer();
        i3.getInfo();
    }
}

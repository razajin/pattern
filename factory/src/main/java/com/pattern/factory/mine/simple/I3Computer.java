package com.pattern.factory.mine.simple;

/**
 * to do
 *
 * @author zhoukaifang
 * @date 2021/12/21
 */
public class I3Computer  implements ComputerFunction {

    private String cpu;
    private String gpu;
    private String memoryBank;

    public void getInfo() {
        System.out.println("info: cpu:" + this.cpu);
    }

    public void open() {

    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getGpu() {
        return gpu;
    }

    public void setGpu(String gpu) {
        this.gpu = gpu;
    }

    public String getMemoryBank() {
        return memoryBank;
    }

    public void setMemoryBank(String memoryBank) {
        this.memoryBank = memoryBank;
    }


    public I3Computer(String cpu, String gpu, String memoryBank) {
        this.cpu = cpu;
        this.gpu = gpu;
        this.memoryBank = memoryBank;
    }
}

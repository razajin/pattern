package com.pattern.factory.mine.factorymethod;


/**
 * to do
 *
 * @author zhoukaifang
 * @date 2021/12/21
 */
public class I7ComputerFactory implements ComputerFactory {
    @Override
    public Computer createComputer() {
        return new I7Computer("i7","580","16G");
    }
}

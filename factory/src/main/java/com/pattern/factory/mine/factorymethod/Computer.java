package com.pattern.factory.mine.factorymethod;

/**
 * to do
 *
 * @author zhoukaifang
 * @date 2021/12/21
 */
public interface Computer {
    void getInfo();

    void open();
}

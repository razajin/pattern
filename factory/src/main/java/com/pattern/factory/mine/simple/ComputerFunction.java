package com.pattern.factory.mine.simple;

/**
 * to do
 *
 * @author zhoukaifang
 * @date 2021/12/21
 */
public interface ComputerFunction {
    void getInfo();

    void open();
}

package com.pattern.factory.mine.simple;

/**
 * to do
 *
 * @author zhoukaifang
 * @date 2021/12/21
 */
public class User {

    public static void main(String[] args) {
        ComputerFactory computerFactory = new ComputerFactory();
        ComputerFunction i3 = computerFactory.createComputer("i3");
        i3.getInfo();
    }
}

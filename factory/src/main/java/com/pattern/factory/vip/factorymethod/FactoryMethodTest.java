package com.pattern.factory.vip.factorymethod;

import com.pattern.factory.vip.ICourse;

/**
 * Created by Tom.
 */
public class FactoryMethodTest {


    /**
     * des.
     * 优点，用户不需要关注创建过程，只需要选择产品即可获得
     * 缺点， 不符合开闭原则  所有的创建都在一个工厂中，增加产品不仅需要增加类，还需要修改这个工厂的创建逻辑
     * @author       : RazaJin
     * @date         : 2021/12/21 17:59
     */
    public static void main(String[] args) {

        ICourseFactory factory = new PythonCourseFactory();
        ICourse course = factory.create();
        course.record();

        factory = new JavaCourseFactory();
        course = factory.create();
        course.record();

    }

}

package com.pattern.factory.vip.factorymethod;

import com.pattern.factory.vip.ICourse;
import com.pattern.factory.vip.PythonCourse;

/**
 * Created by Tom.
 */
public class PythonCourseFactory implements ICourseFactory {

    public ICourse create() {
        return new PythonCourse();
    }
}

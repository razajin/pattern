package com.pattern.factory.vip.factorymethod;

import com.pattern.factory.vip.ICourse;
import com.pattern.factory.vip.JavaCourse;

/**
 * Created by Tom.
 */
public class JavaCourseFactory implements ICourseFactory {
    public ICourse create() {
        return new JavaCourse();
    }
}

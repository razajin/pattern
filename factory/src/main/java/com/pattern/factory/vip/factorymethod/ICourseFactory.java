package com.pattern.factory.vip.factorymethod;

import com.pattern.factory.vip.ICourse;

/**
 * 工厂模型
 * Created by Tom.
 */
public interface ICourseFactory {

    ICourse create();

}

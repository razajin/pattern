package com.pattern.factory.vip.abstractfactory;

/**
 * 课堂笔记
 * Created by Tom.
 */
public interface INote {
    void edit();
}

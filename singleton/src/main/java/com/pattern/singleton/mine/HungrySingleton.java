package com.pattern.singleton.mine;

import lombok.Data;

import java.util.Date;

/**
 * to do
 *
 * @author zhoukaifang
 * @date 2021/12/27
 */
@Data
public class HungrySingleton {

    private Date createTime;

    private static final HungrySingleton HUNGRY_SINGLETON = new HungrySingleton();

    private HungrySingleton(){
        createTime = new Date();
    }

    private static HungrySingleton getInstance(){
        return HUNGRY_SINGLETON ;
    }

    public static void main(String[] args) {

    }

}
